<?php
include("../form-functions.php");
  //for start session
@startsession();
$base_url = siteURL();
$base_url =$base_url.'/landing/residential';
include('mailsendclass.php');
$email_to = "sales@cspalarms.ca";

if($_POST['titleForm1']){
	$sitename = $_POST['titleForm1']; 
}elseif($_POST['titleForm2']){
	$sitename = $_POST['titleForm2'];
}
else{
	$sitename = $_POST['titleForm3']; 
} 



/*echo "<pre>";
 print_r($_POST);
echo "</pre>";

exit;*/
define('SITE_URL',$base_url);
$recaptcha_id = $_POST['recaptcha_id'];

$allowed_origins = array('https://www.cspalarms.ca', 'http://www.cspalarms.ca', 'https://www.cspalarms.ca/landing', 'http://www.cspalarms.ca/landing');
if(isset($_SERVER['HTTP_ORIGIN']) && !in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) { ?>
	<script language="javascript">
 		alert('Invalid Data. Cannot Proceed.');
 		window.location='<?php echo SITE_URL ?>';
	</script>
	<?php
 	exit;	
}
else if(strpos($_SERVER['HTTP_REFERER'], 'https://www.cspalarms.ca') !== 0 && strpos($_SERVER['HTTP_REFERER'], 'http://www.cspalarms.ca') !== 0) {
    ?>
	<script language="javascript">
 		alert('Invalid Data. Cannot Proceed.');
 		window.location='<?php echo SITE_URL ?>';
	</script>
	<?php
 	exit;	
}
else if($_SERVER['REQUEST_METHOD']==='GET') {
    ?>
	<script language="javascript">
 		alert('Invalid Data. Cannot Proceed.');
 		window.location='<?php echo SITE_URL ?>';
	</script>
	<?php
 	exit;	
}
else{
		if((isset($_REQUEST['field_name']) && $_REQUEST['field_name']!='') || (isset($_REQUEST['field_email']) && $_REQUEST['field_email']!='') ||(isset($_REQUEST['field_phone']) && $_REQUEST['field_phone']!=''))
	{
		?>
	  <script language="javascript">
	    alert('Invalid Data. Cannot Proceed.');
	    window.location='<?php echo SITE_URL; ?>';
	  </script>
	  <?php
	  exit();
	}


################################################################
if($_POST['recaptchaResponse'.$recaptcha_id]){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, [
	   'secret' => "6Lc1peAeAAAAAPx0asRm5xzDQwBIYjIxxw4IN5Ip",
	   'response' => $_POST['recaptchaResponse'.$recaptcha_id],
	   'remoteip' => $_SERVER['REMOTE_ADDR']
	]);
}

$resp = json_decode(curl_exec($ch));
curl_close($ch);
if($resp->score >= 0.5)
{	
	if(isset($_REQUEST['formHidId1']) && $_REQUEST['formHidId1']=='first-form')
	{
		$form_id =  $_REQUEST['formHidId1'];
		if( $_POST['afldToken']==''|| $_POST['aToken']=='')
		{
			?>
			<script language="javascript">
			alert('Invalid Data. Cannot Proceed.');
			window.location='<?php echo SITE_URL; ?>';
			</script>
			<?php
			exit();
		}
		else
		{
		$afldToken = $_POST['afldToken'];
		$token = $_POST['aToken'];
		
		$name=stripslashes($_POST['aname'.$afldToken]);
		$email=stripslashes($_POST['aemail'.$afldToken]);
		$phone=stripslashes($_POST['aphone'.$afldToken]);
		$city=$_POST['acity'.$afldToken];
		$comments=$_POST['acomment'.$afldToken];
		$email_fieldname=$_POST['aemail'.$afldToken];
		$phone_fieldname=$_POST['aphone'.$afldToken];

		$array=array(
			'aname'.$afldToken => "Full Name",
			'aemail'.$afldToken => "Email Address",
			'aphone'.$afldToken => "Phone Number",
			'acity'.$afldToken => "City",
			'acomment'.$afldToken=> "Questions/Comments",	
		);
		$form_name = $_POST['formName1'];
		$subject = $form_name." - " .$sitename;
		}
	}
	if(isset($_REQUEST['formHidId2']) && $_REQUEST['formHidId2']=='second-form')
	{
		$form_id =  $_REQUEST['formHidId2'];
		if( $_POST['bfldToken']==''|| $_POST['bToken']=='')
		{
			?>
			<script language="javascript">
				alert('Invalid Data. Cannot Proceed.');
				window.location='<?php echo SITE_URL; ?>';
			</script>
			<?php
			exit();
		}
		else
		{
		$bfldToken = $_POST['bfldToken'];
		$token = $_POST['bToken'];
		
		$name=stripslashes($_POST['bname'.$bfldToken]);
		$email=stripslashes($_POST['bemail'.$bfldToken]);
		$phone=stripslashes($_POST['bphone'.$bfldToken]);
		$city=$_POST['bcity'.$bfldToken];
		$need_alarm = $_POST['need_alarm'];
		$home_owner = $_POST['home_owner'];
		$comments=$_POST['bcomment'.$bfldToken];
		$email_fieldname=$_POST['bemail'.$bfldToken];
		$phone_fieldname=$_POST['bphone'.$bfldToken];
		$array=array(
			'bname'.$bfldToken => "Name",
			'bemail'.$bfldToken => "Email Address",
			'bphone'.$bfldToken => "Phone Number",
			'bcity'.$bfldToken => "City",
			'need_alarm' => "Do you currently have an alarm system?",
			'home_owner' => "Are you the home owner?",
			'bcomment'.$bfldToken=> "Questions/Comments",		
		);
		$form_name = $_POST['formName2'];
		$other_fields = array('Do you currently have an alarm system?'=>$need_alarm,'Are you the home owner?'=>$home_owner);
		$subject = $form_name." - " .$sitename;		
		}
	}
	if(isset($_REQUEST['formHidId3']) && $_REQUEST['formHidId3']=='popUp_form')
	{
		$form_id =  $_REQUEST['formHidId3'];
		if( $_POST['cfldToken']==''|| $_POST['cToken']=='')
		{
			?>
			<script language="javascript">
				alert('Invalid Data. Cannot Proceed.');
				window.location='<?php echo SITE_URL; ?>';
			</script>
			<?php
			exit();
		}
		else
		{
		$cfldToken = $_POST['cfldToken'];
		$token = $_POST['cToken'];
		
		$name=stripslashes($_POST['cname'.$cfldToken]);
		$email=stripslashes($_POST['cemail'.$cfldToken]);
		$phone=stripslashes($_POST['cphone'.$cfldToken]);		
		$comments=$_POST['ccomment'.$cfldToken];
		$email_fieldname=$_POST['cemail'.$cfldToken];
		$phone_fieldname=$_POST['cphone'.$cfldToken];
		$array=array(
			'cname'.$cfldToken => "Name",
			'cemail'.$cfldToken => "Email",
			'cphone'.$cfldToken => "Phone",
			
			'ccomment'.$cfldToken=> "Questions/Comments",		
		);
		$form_name = $_POST['formName3'];
		$subject = $form_name." - " .$sitename;
		}
	}
}else{ 
	?>
	<script language="javascript">
		alert('Invalid Data. Cannot Proceed.');
		window.location='<?php echo SITE_URL; ?>';
	</script>
	<?php
	exit();
}


//Replacing the to, cc and bcc email address if @techwyseintl.com is found STARTS HERE  
	if($name!="")
	{
		$email=stripslashes($email);
		$email_string = strstr($email, "@techwyseintl.com"); //Checking whether email string contains @techwyseintl.com
		$to = ($email_string=="@techwyseintl.com")? stripslashes($email) : $email_to;
		$bcc_email = ($email_string=="@techwyseintl.com")? "" : $email_bcc;
		$cc_email = ($email_string=="@techwyseintl.com")? "" : $email_cc;
	
		$con_subject="Confirmation - " .$sitename;
	}


//  spam check using https://check-mail.org/
$curl12 = curl_init();

curl_setopt_array($curl12, [
	CURLOPT_URL => "https://mailcheck.p.rapidapi.com/?domain=".$email,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"X-RapidAPI-Host: mailcheck.p.rapidapi.com",
		"X-RapidAPI-Key: de9217b3b1msh21960afabfecb21p157ae7jsnd5a1f59b671b"
	],
]);

$spam_response = curl_exec($curl12);
$err = curl_error($curl12);

curl_close($curl12);

// if ($err) {
// 	echo "cURL Error #:" . $err;
// } else {
// 	echo $response;
// }
$data_json = json_decode($spam_response);


if($data_json->block){
	// date_default_timezone_set($timezone_name);
	// 
	date_default_timezone_set('Canada/Eastern');

	foreach( $array as $key => $value)
	{
		$data[] = $_POST[$key];
	}
	$data[] = $_SERVER['REMOTE_ADDR'];
	$data[] = date("Y-m-d h:i:s");
	$data[] = $_SERVER['HTTP_USER_AGENT'];
	$data[] = $_SERVER['HTTP_REFERER'];

	$data_comb = implode(' | ', $data);

	$file = 'spamlead/spamlead-'.date("d-m-y").'.txt';
	// Open the file to get existing content
	$current = file_get_contents($file);
	// Append a new person to the file
	$current .= $data_comb."\n\n";
	// Write the contents back to the file
	file_put_contents($file, $current);
	echo "<script>window.location.href='".$base_url."/thank-you.php';</script>";
}
else{




//  spam check using https://check-mail.org/


if($name!='')
{ 
$obj = new formTempclass();
$obj->name 		            = $name; // This is mantatory- Field name of name
$obj->email_fieldname 		= $email_fieldname; // This is mantatory- Field name of Email which send to user 
$obj->phone_fieldname 		= $phone_fieldname; // This is mantatory- Field name of Phone  which send to user
//$obj->captcha_fieldname 	= $captcha_fieldname; // This is mantatory- Field name of Captcha  which send to user
//$obj->session_fieldname 	= $session_fieldname; // This is mantatory- Field name of Session  which send to user
$obj->postval_array	        = $array;

$obj->sitename 				= $sitename; //Site Name
$obj->siteurl 				= $base_url;  // Site Url 
$obj->mail_banner 			= $base_url."/mails/images/mail-template.jpg"; // path of mail image 
$obj->rowbgcolor   			= "#fff"; // Bg color of each row
$obj->rowsubtitlebgcolor	= "#363435"; // Bg color of titles like persoonal Details
$obj->footerbgcolor			= "#363435";
$obj->footer_txtcolor		= "#fff";
$obj->bordercolor 			= "#000";

$obj->admin_mailto 			= $to;
$obj->admin_bcc_mail		= $bcc_email;
$obj->admin_cc_mail 		= $cc_email;
$obj->admin_subject 		= $subject;
$obj->con_subject			= $con_subject; // Confirmation mail Sublect
$obj->send_to_user          = true;
$obj->AdminmailSend(); 

//--- uncomment for view the tempahtes in browser

$body = $obj->UserTemplate($array);
$bodyadmin = $obj->adminTemplate($array);

/*  print_r($_POST);
  print_r($body);
  echo "<br>";
  print_r($bodyadmin);*/

// exit;
//--- uncomment for view the tempahtes in browser
//--------------------------------------adluge code--------------------------------------
	require_once("clientcenter-api-library.php"); 
	$lead = new clientcenter();
	$lead->client_code="531b79c40c1e292f36432dac97b4f55a"; // Mandatory. Unique identification code.

	$lead->fname=$name; //post  Value of first name \
	$lead->email=$email;//post  Value of Email addess 
	$lead->phone=$phone;//post Value of Phone Number 
	$lead->comments=$comments;//post Value of Comments
	$lead->city=$city;
	//$lead->postalcode=$postalcode; //post  Value of first name 
	$lead->other_fields=json_encode($other_fields);//post Value of other_fields
	$lead->status=1; // No need to change this
	
	//No need to modify any of the below code
	$lead->useragent = $_SERVER['HTTP_USER_AGENT']; //browser properties
	$lead->remote_ip=$_SERVER['REMOTE_ADDR']; //ip address
	$lead->referrer=$_SERVER['HTTP_REFERER'];// page source
	$lead->contact_date=date("Y-m-d h:i:s");
	$lead->search_engine=$_COOKIE['adl_durl'];
	$lead->keyword=$_COOKIE['adl_key'];
	$lead->source=$_COOKIE['adl_camp'];
	$lead->randomnum=$_COOKIE['adl_rand'];
	$lead->adl_ref=$_COOKIE['adl_ref'];
		
	$lead->send_to_adluge=true; // Set to true If you are sending leads to adluge //default true
	$lead->send();
	// //echo "thank-you";exit;
//--------------------------------------------------------------------------------------------------

	echo "<script>window.location.href='".$base_url."/thank-you.php';</script>";


}
else
{
echo "<script>window.location.href='".$base_url."';</script>";
}
}
}
?>
